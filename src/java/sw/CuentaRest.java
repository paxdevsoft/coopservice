/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sw;

import datos.CrudService;
import herramientas.Hasher;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import modelo.AuthUser;
import modelo.AuthUserGroups;
import modelo.Cliente;
import modelo.Cuenta;
import modelo.Transaccion;

/**
 *
 * @author camila
 */
@Path("cuenta")
public class CuentaRest {

    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response crearCuenta(Cuenta cuenta) {
        try {
            new CrudService().create(cuenta);
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("estado", "creado");
            jsonObjectBuilder.add("cuenta", "" + cuenta);
            JsonObject jsonObject = jsonObjectBuilder.build();
            return Response.ok(jsonObject.toString()).build();

        } catch (Exception e) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("estado", "no creado");
            jsonObjectBuilder.add("error", e + "");
            JsonObject jsonObject = jsonObjectBuilder.build();
            return Response.ok(jsonObject.toString()).build();
        }
    }

    @GET
    @Path("/buscarCedula")
    @Produces({"application/json", "application/json"})
    public Response buscarCedula(@QueryParam("cedula") String cedula) {
        List<Cliente> cliente = new CrudService().findWithQuery("SELECT e FROM Cliente e WHERE e.cedula = '" + cedula + "'");
        List<Cuenta> cuenta = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE e.clienteId=" + cliente.get(0).getClienteId() + "");
        if (!cuenta.isEmpty()) {
            return Response.ok(cuenta.get(0)).build();
        } else {
            return Response.noContent().build();
        }
    }

    @Path("/modificarCliente")
    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response modificarCliente(Cliente cliente) {
        List<Cliente> est = new CrudService().findWithQuery("SELECT e FROM Cliente e WHERE e.cedula = '" + cliente.getCedula() + "'");
        Cliente cl = est.get(0);
        cl.setApellidos(cliente.getApellidos());
        cl.setNombres(cliente.getNombres());
        cl.setEstadoCivil(cliente.getEstadoCivil());
        cl.setCorreo(cliente.getCorreo());
        cl.setTelefono(cliente.getTelefono());
        cl.setCelular(cliente.getCelular());
        cl.setDireccion(cliente.getDireccion());
        CrudService crudService = new CrudService();
        crudService.update(cl);
        return Response.ok(cl).build();
    }

    @Path("/guardarCliente")
    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response guardarCliente(JsonObject jsonReq) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String FechaNacimiento = jsonReq.getString("fechaNacimiento");
        Date dateNacimiento = new Date();
        try {
            dateNacimiento = formatter.parse(FechaNacimiento);
        } catch (ParseException ex) {
            Logger.getLogger(CuentaRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Cliente cl = new Cliente();
        cl.setCedula(jsonReq.getString("cedula"));
        cl.setApellidos(jsonReq.getString("apellidos"));
        cl.setNombres(jsonReq.getString("nombres"));
        cl.setEstadoCivil(jsonReq.getString("estadoCivil"));
        cl.setCorreo(jsonReq.getString("correo"));
        cl.setTelefono(jsonReq.getString("telefono"));
        cl.setCelular(jsonReq.getString("celular"));
        cl.setGenero(jsonReq.getString("genero"));
        cl.setFechaNacimiento(dateNacimiento);
        cl.setDireccion(jsonReq.getString("direccion"));
        cl.setEstado(true);
        Cuenta ct = new Cuenta();
        ct.setEstado(true);
        ct.setFechaApertura(date);
        ct.setNumero(cl.getCedula() + this.random() + this.random() + this.random());
        ct.setSaldo(BigDecimal.valueOf(Double.parseDouble(jsonReq.getString("saldo"))));
        ct.setTipoCuenta(jsonReq.getString("tipoCuenta"));
        ct.setClienteId(cl);
        new CrudService().create(cl);
        new CrudService().create(ct);
        return Response.ok(ct).build();
    }

    private int random() {
        return (int) (Math.random() * 9);
    }

    @Path("/dos")
    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public JsonObject dos(JsonArray jsonArray) {
        return jsonArray.getJsonObject(2);
    }

    @Path("/lista")
    @GET
    @Produces({"application/json", "application/json"})
    public List<Cliente> listaClientes() {
        List<Cliente> lista = new CrudService().findWithQuery("SELECT c FROM Cliente c WHERE c.estado=true");
        return lista;
    }

    @Path("/cambiarEstado")
    @GET
    //@Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response cambiarEstado(@QueryParam("numero") String numero, @QueryParam("estado") boolean estado) {
        //CrudService est = new CrudService();

        List<Cuenta> esl = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE e.numero = '" + numero + "'");
        System.out.println(numero);
        if (esl.isEmpty()) {
            return Response.ok("{\"msg\":\"NO EXISTE LA CUENTA\"}").build();

        }

        Cuenta ct = esl.get(0);
        List<Cliente> esp = new CrudService().findWithQuery("SELECT c FROM Cliente c WHERE c.clienteId = " + ct.getClienteId().getClienteId());
        if (estado) {

            Cliente cl = esp.get(0);
            cl.setEstado(true);
            ct.setEstado(true);
//            est.update(cl);
//            est.update(ct);
            new CrudService().update(ct);
            new CrudService().update(cl);

            return Response.ok("{\"msg\":\"MODIFICADO ESTADO\"}").build();
        } else {

            ct.setEstado(false);
            new CrudService().update(ct);
            List<Cuenta> cuentas = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE e.estado = true and e.clienteId = " + esp.get(0).getClienteId());
            if (cuentas.size() <= 0) {
                Cliente cl = esp.get(0);
                cl.setEstado(false);
                new CrudService().update(cl);
            }
            return Response.ok("{\"msg\":\"MODIFICADO EL ESTADO DE CUENTAS\"}").build();

        }
    }

    @GET
    @Path("/buscarCuenta")
    @Produces({"application/json", "application/json"})
    public Response buscarCuenta(@QueryParam("numero") String numero) {
        List<Cuenta> cuenta = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE e.numero = '" + numero + "'");
        if (!cuenta.isEmpty()) {
            return Response.ok(cuenta.get(0)).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("/buscarTransaccion")
    @Produces({"application/json", "application/json"})
    public List<Transaccion> buscarTransaccion(@QueryParam("cedula") String cedula) {
        List<Cliente> cliente = new CrudService().findWithQuery("SELECT e FROM Cliente e WHERE cedula = '" + cedula + "'");
        List<Cuenta> cuenta = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE cliente_id = '" + cliente.get(0).getClienteId() + "'");
        List<Transaccion> transaccion = new CrudService().findWithQuery("SELECT e  FROM Transaccion e WHERE cuenta_id = '" + cuenta.get(0).getCuentaId() + "'");
        if (!transaccion.isEmpty()) {
            return transaccion;
        } else {
            return null;
        }
    }

    @Path("/transferencia")
    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response transferencia(JsonObject jsonReq) {
        List<Cuenta> listacuentaR = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE e.numero='" + jsonReq.getString("numeroR") + "'");
        List<Cuenta> listacuentaD = new CrudService().findWithQuery("SELECT e FROM Cuenta e WHERE e.numero='" + jsonReq.getString("numeroD") + "'");
        if (!listacuentaR.isEmpty() && !listacuentaD.isEmpty()) {
            Date date = new Date();
            Cuenta cuentaR = listacuentaR.get(0);
            Cuenta cuentaD = listacuentaD.get(0);
            BigDecimal saldo = cuentaR.getSaldo();
            BigDecimal saldoD = cuentaD.getSaldo();
            if (cuentaR.getNumero() != cuentaD.getNumero()) {
                if (saldo.intValue() > 0) {
                    BigDecimal valor = BigDecimal.valueOf(Double.parseDouble(jsonReq.getString("valor")));
                    if (saldo.intValue() >= valor.intValue()) {
                        cuentaR.setSaldo(saldo.subtract(valor));
                        new CrudService().update(cuentaR);
                        cuentaD.setSaldo(saldoD.add(valor));
                        new CrudService().update(cuentaD);
                        Transaccion transaccion = new Transaccion();
                        transaccion.setFecha(date);
                        transaccion.setTipo("Transferencia");
                        transaccion.setValor(valor);
                        transaccion.setDescripcion(jsonReq.getString("descripcion"));
                        transaccion.setResponsable(jsonReq.getString("responsable"));
                        transaccion.setCuentaId(cuentaR);
                        return Response.ok(new CrudService().create(transaccion)).build();
                    }
                }
                return Response.noContent().build();
            }
            return Response.noContent().build();
        } else {
            return Response.noContent().build();
        }
    }

    @Path("/deposito")
    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response deposito(JsonObject jsonReq) {
        List<Cuenta> listCuenta = new CrudService().findWithQuery("SELECT c FROM Cuenta c WHERE c.numero = '" + jsonReq.getString("numero") + "'");
        if (!listCuenta.isEmpty()) {
            Date date = new Date();
            Cuenta cuenta = listCuenta.get(0);
            BigDecimal valor = BigDecimal.valueOf(Double.parseDouble(jsonReq.getString("valor")));
            BigDecimal saldo = cuenta.getSaldo();
            cuenta.setSaldo(saldo.add(valor));
            new CrudService().update(cuenta);
            Transaccion transaccion = new Transaccion();
            transaccion.setFecha(date);
            transaccion.setTipo("deposito");
            transaccion.setValor(valor);
            transaccion.setDescripcion(jsonReq.getString("descripcion"));
            transaccion.setResponsable(jsonReq.getString("responsable"));
            transaccion.setCuentaId(cuenta);
            return Response.ok(new CrudService().create(transaccion)).build();
        } else {
            return Response.noContent().build();
        }
    }

    @Path("/retiro")
    @POST
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response retiro(JsonObject jsonReq) {
        List<Cuenta> listCuenta = new CrudService().findWithQuery("SELECT c FROM Cuenta c WHERE c.numero = '" + jsonReq.getString("numero") + "'");
        if (!listCuenta.isEmpty()) {
            Date date = new Date();
            Cuenta cuenta = listCuenta.get(0);
            BigDecimal saldo = cuenta.getSaldo();
            if (saldo.intValue() > 0) {
                BigDecimal valor = BigDecimal.valueOf(Double.parseDouble(jsonReq.getString("valor")));
                if (saldo.intValue() >= valor.intValue()) {
                    cuenta.setSaldo(saldo.subtract(valor));
                    new CrudService().update(cuenta);
                    Transaccion transaccion = new Transaccion();
                    transaccion.setFecha(date);
                    transaccion.setTipo("retiro");
                    transaccion.setValor(valor);
                    transaccion.setDescripcion(jsonReq.getString("descripcion"));
                    transaccion.setResponsable(jsonReq.getString("responsable"));
                    transaccion.setCuentaId(cuenta);
                    return Response.ok(new CrudService().create(transaccion)).build();
                }
            }
            return Response.noContent().build();
        } else {
            return Response.noContent().build();
        }
    }

//LOGIN
    @POST
    @Path("/buscarUsuariosData")
    @Consumes({"application/json", "application/json"})
    @Produces({"application/json", "application/json"})
    public Response buscarUsuarios(JsonObject jsonReq) {
        List<AuthUser> listaUsuario = new CrudService().findWithQuery("SELECT e FROM AuthUser e WHERE e.username='" + jsonReq.getString("nombre") + "'");
        if (listaUsuario.isEmpty()) {
            return Response.noContent().build();
        } else {
            List<AuthUserGroups> listaGrupo = new CrudService().findWithQuery("SELECT e FROM AuthUserGroups e WHERE e.userId=" + listaUsuario.get(0).getId());

            Hasher validar = new Hasher();
            boolean validarClave = validar.passwordShouldMatch(jsonReq.getString("clave"), listaUsuario.get(0).getPassword());

            if (validarClave) {

                return Response.ok(listaGrupo.get(0)).build();

            } else {
                return Response.noContent().build();
            }
        }
    }

}
